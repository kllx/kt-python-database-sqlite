import os

from flask import Flask, render_template, request, redirect
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

app = Flask(__name__)

basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'cars.sqlite3')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
migrate = Migrate(app, db)

class Market(db.Model):
    __tablename__ = 'markets'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), index=False, unique=False)
    balance = db.Column(db.Float)
    cars = db.relationship('Car', backref='markets', lazy=True, uselist=True)
class Car(db.Model):
    __tebname__ = 'cars'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), index=False, unique=False)
    price = db.Column(db.Float)
    market_id = db.Column(db.Integer, db.ForeignKey('markets.id'))
    person_id = db.Column(db.Integer, db.ForeignKey('person.id'))

class Person(db.Model):
    __tebname__ = 'persons'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), index=False, unique=False)
    balance = db.Column(db.Float)
    cars = db.relationship('Car', backref='person', lazy=True, uselist=True)


@app.route('/', methods=['GET'])
def index():
    return render_template('index.html', markets=Market.query.all(), persons=Person.query.all())

@app.route('/market/add', methods=['GET', 'POST'])
def market_add():
    if request.method == 'GET':
        return render_template('add_market.html')
    elif request.method == 'POST':
        market = Market()
        market.name = request.form['market_name']
        market.balance = float(request.form['market_balance'])
        db.session.add(market)
        db.session.commit()
        return redirect('/')

@app.route('/market/<int:id>/edit', methods=['GET', 'POST'])
def market_edit(id):
    market = Market.query.filter_by(id=id).first()

    if request.method == 'GET':
        return render_template('edit_market.html', market=market)
    elif request.method == 'POST':
        market.name = request.form['market_name']
        db.session.add(market)
        db.session.commit()
        return redirect('/market/{}'.format(market.id))

@app.route('/market/<int:id>/addcar', methods=['GET', 'POST'])
def add_car_to_market(id):
    market = Market.query.filter_by(id=id).first()
    if request.method == 'GET':
        return render_template('add_car_to_market.html', market=market)
    elif request.method == 'POST':
        car = Car.query.filter_by(id=request.form['car_id']).first()
        car.person_id = None
        market.cars.append(car)
        db.session.add(car)
        db.session.add(market)
        db.session.commit()
        return redirect('/market/{}'.format(id))

@app.route('/market/<int:id>', methods=['GET'])
def market_info(id):
    return render_template('market.html', market=Market.query.filter_by(id=id).first())

@app.route('/car/add', methods=['GET', 'POST'])
def car_add():
    if request.method == 'GET':
        return render_template('add_car.html')
    elif request.method == 'POST':
        car = Car()
        car.name = request.form['car_name']
        car.price = float(request.form['car_price'])
        db.session.add(car)
        db.session.commit()
        return redirect('/')

@app.route('/car/<int:id>')
def info_car(id):
    return render_template('car.html', car=Car.query.filter_by(id=id).first())

@app.route('/person/add', methods=['GET', 'POST'])
def person_add():
    if request.method == 'GET':
        return render_template('add_person.html')
    elif request.method == 'POST':
        person = Person()
        person.name = request.form['person_name']
        person.balance = float(request.form['person_balance'])
        db.session.add(person)
        db.session.commit()
        return redirect('/')

@app.route('/person/<int:id>')
def person(id):
    return render_template('person.html', person=Person.query.filter_by(id=id).first())

@app.route('/person/<int:id>/addcar', methods=['GET', 'POST'])
def person_addCar(id):
    person = Person.query.filter_by(id=id).first()
    if request.method == 'GET':
        return render_template('add_car_to_person.html', person=person)
    elif request.method == 'POST':
        car = Car.query.filter_by(id=request.form['car_id']).first()
        car.market_id = None
        person.cars.append(car)
        db.session.add(car)
        db.session.add(person)
        db.session.commit()
        return redirect('/person/{}'.format(id))

if __name__ == '__main__':
    app.run()